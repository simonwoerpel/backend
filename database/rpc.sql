create or replace function fg.search_articles(date_order text, search_title text, search_abstract text)
  returns json
  language plpython3u
as $$
    import json

    params = []
    ptypes = []

    sql_query = """
      select doi, title, abstract, pdate, coi_text, fund_text from fg.article
        join fg.coi on fg.coi.article_id = fg.article.id
        join fg.pub_date on fg.pub_date.article_id = fg.article.id where fg.pub_date.ptype = 'epub'"""

    plpy.info('SEARCH', search_title, date_order)
    if search_title != '':
      sql_query += " and fg.article.title_search @@ to_tsquery($1)"
      params += ['%{}%'.format(search_title)]
      ptypes += ['text']

    if search_abstract:
      sql_query += " and fg.article.abstract_search @@ to_tsquery(${})".format(len(params) + 1)
      params += ['%{}%'.format(search_abstract)]
      ptypes += ['text']

    # sql_query += " order by fg.pub_date.pdate {} limit ${};".format(date_order, len(params) + 1)
    # params += [10]
    # ptypes += ["int"]

    plpy.info(sql_query, params)

    query = plpy.prepare(sql_query, ptypes)
    result = plpy.execute(query, params)

    rows = []
    for r in result:
      rows += [r]

    return json.dumps(rows)
$$;

create or replace function fg.author_articles(author_id int)
  returns json
  language plpython3u
as $$
    import json

    sql_query = """
       select doi, title, abstract, pdate, coi_text, fund_text from fg.article
         join fg.coi on fg.coi.article_id = fg.article.id
         join fg.pub_date on fg.pub_date.article_id = fg.article.id
 	and fg.article.id in (select article_id from fg.contribution where contributor_id = $1) 
 	order by fg.pub_date.pdate desc"""
 
 
    query = plpy.prepare(sql_query, ["int"])
    result = plpy.execute(query, [author_id])

    json_result = {'articles': []}

    for r in result:
      json_result['articles'] += [r]

    sql_author_query = 'select name,surname from fg.contributor where id = $1'
    query = plpy.prepare(sql_author_query, ["int"])
    result = plpy.execute(query, [author_id])

    json_result['author'] = {'name': result[0]['name'], 'surname': result[0]['surname']}

    return json.dumps(json_result)
$$;
